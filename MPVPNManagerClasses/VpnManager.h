//
//  VpnManager.h
//  MPVPNManager
//
//  Created by mopellet on 2017/7/6.
//  Copyright © 2017年 mopellet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VpnManager : NSObject
+ (instancetype)shareInstance;
- (void)connect;
- (void)disconnect;
// 注意设置路由信息
@end

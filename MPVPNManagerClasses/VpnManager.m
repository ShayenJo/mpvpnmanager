//
//  VpnManager.m
//  MPVPNManager
//
//  Created by mopellet on 2017/7/6.
//  Copyright © 2017年 mopellet. All rights reserved.
//

#import "VpnManager.h"
#import <NetworkExtension/NetworkExtension.h>
@implementation VpnManager

+ (instancetype)shareInstance
{
    static id instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        
            [self loadProviderManager:^(NETunnelProviderManager *manager) {
                if (manager) {
                    [self updateVPNStatus:manager];
                }
            }];
        
        [self addVPNStatusObserver];
    }
    return self;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadProviderManager:(void(^)(NETunnelProviderManager *manager))complete {
    [NETunnelProviderManager loadAllFromPreferencesWithCompletionHandler:^(NSArray<NETunnelProviderManager *> * _Nullable managers, NSError * _Nullable error) {
        if (managers.count) {
            complete([managers firstObject]);
            return ;
        }
        complete(nil);
    }];
}

- (void)addVPNStatusObserver {
    [self loadProviderManager:^(NETunnelProviderManager *manager) {
        if (manager) {
            [[NSNotificationCenter defaultCenter] addObserverForName:NEVPNStatusDidChangeNotification
                                                              object:manager.connection
                                                               queue:[NSOperationQueue mainQueue]
                                                          usingBlock:^(NSNotification * _Nonnull note) {
                                                              [self updateVPNStatus:manager];
                                                          }];
        }
    }];
}

- (void)updateVPNStatus:(NEVPNManager *)manager {
    switch (manager.connection.status) {
        case NEVPNStatusInvalid:
            NSLog(@"NEVPNStatusInvalid The VPN is not configured.");
            break;
        case NEVPNStatusDisconnected:
            NSLog(@"NEVPNStatusDisconnected The VPN is disconnected.");
            break;
        case NEVPNStatusConnecting:
            NSLog(@"NEVPNStatusConnecting The VPN is connecting.");
            break;
        case NEVPNStatusConnected:
            NSLog(@"NEVPNStatusConnected The VPN is connected.");
            break;
        case NEVPNStatusReasserting:
            NSLog(@"NEVPNStatusReasserting The VPN is reconnecting following loss of underlying network connectivity.");
            break;
        case NEVPNStatusDisconnecting:
            NSLog(@"NEVPNStatusDisconnecting The VPN is disconnecting.");
            break;
        default:
            break;
    }
}


- (NETunnelProviderManager *)createProviderManager {
    NETunnelProviderManager *manager = [NETunnelProviderManager new];
    NETunnelProviderProtocol *conf = [NETunnelProviderProtocol new];
    conf.serverAddress = @"MPVPN";
    manager.protocolConfiguration = conf;
    manager.localizedDescription = @"MPVPN";
    return manager;
}

- (void)loadAndCreatePrividerManager:(void(^)(NETunnelProviderManager *manager))complete {
    [NETunnelProviderManager loadAllFromPreferencesWithCompletionHandler:^(NSArray<NETunnelProviderManager *> * _Nullable managers, NSError * _Nullable error) {
        NETunnelProviderManager *manager;
        if (managers.count) {
            manager = [managers firstObject];
            [self delDupConfig:managers];
        }
        else {
            manager = [self createProviderManager];
        }
        
        manager.enabled = YES;
        [self setRulerConfig:manager];
        [manager saveToPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
            if (error != nil){
                complete(nil);
                return;
            }
            
            [manager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                if (error != nil){
                    NSLog(@"%@",error.debugDescription);
                    complete(nil);
                    return;
                }
                
                [self addVPNStatusObserver];
                complete(manager);
            }];
        }];
        
    }];
}


- (void)delDupConfig:(NSArray <NETunnelProviderManager *> *)arrays {
    for (NETunnelProviderManager *manager in arrays) {
        NSLog(@"Del DUP Profiles");
        [manager removeFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
            if(error != nil){
                NSLog(@"%@",error.debugDescription);
            }
        }];
    }
}

- (NSString *)getRuleConf {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"NEKitRule" ofType:@"conf"];
    NSData *data = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:path]];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return str;
}

#warning set config
- (void)setRulerConfig:(NETunnelProviderManager *)manager {
    NSDictionary *conf = @{@"ss_address":@"ss_address",
                           @"ss_port":@(1025),
                           @"ss_method":@"CHACHA20",
                           @"ss_password":@"ss_password",
                           @"ymal_conf":[self getRuleConf],
                           };
    
    NETunnelProviderProtocol * orignConf = (NETunnelProviderProtocol*)manager.protocolConfiguration;
    orignConf.providerConfiguration = conf;
    manager.protocolConfiguration = orignConf;
}


- (void)connect {
    [self loadAndCreatePrividerManager:^(NETunnelProviderManager *manager) {
        if (manager) {
            /*
            NETunnelProviderSession *session = (NETunnelProviderSession*) manager.connection;
            NSDictionary *options = @{@“key” : @“value”};// Send additional options to the tunnel provider
            NSError *err;
            [session startTunnelWithOptions:options andReturnError:&err];
             */
            NSError *error;
            [manager.connection startVPNTunnelAndReturnError:&error];
        }
    }];
}

- (void)disconnect {
    [self loadProviderManager:^(NETunnelProviderManager *manager) {
        if (manager) {
            [manager.connection stopVPNTunnel];
        }
    }];
}

@end

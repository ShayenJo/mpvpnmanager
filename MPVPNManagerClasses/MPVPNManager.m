
#import "MPVPNManager.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>
//#import "MPVPNIKEv2Config.h"
//#import "NEVPNProtocolL2TP.h"
#import "MPCommon.h"
#include <dlfcn.h>
#import <ifaddrs.h>
#import <MJExtension/MJExtension.h>

@implementation MPVPNConfigInfo
MJCodingImplementation
@end

@interface MPVPNManager ()
@property (nonatomic, strong) NEVPNManager * vpnManager;
/** config info */
@property (nonatomic, readonly, strong) MPVPNConfigInfo * _Nullable config;
@end

@implementation MPVPNManager

+ (instancetype)shareInstance
{
    static id instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.vpnManager = [NEVPNManager sharedManager];
        [self performSelector:@selector(registerNetWorkReachability) withObject:nil afterDelay:0.35f];
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) createKeychainPassword:(NSString *)password privateKey:(NSString *)privateKey
{
    if (password.length) {
         [MPCommon createKeychainValue:password forIdentifier:MPVPNPasswordIdentifier];
    }
   
    if (privateKey.length) {
        [MPCommon createKeychainValue:privateKey forIdentifier:MPVPNSharePrivateKeyIdentifier];
    }
   
}

- (void)checkVPNConfigCompleteHandler:(MPVPNManagerCompletionHandler _Nonnull )completion {
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"加载 VPN 偏好设置失败 : %@", error);
            completion ? completion(NO, error) : 0;
        } else {
            if ([[NSString stringWithFormat:@"%@", _vpnManager.protocol] rangeOfString:@"persistentReference"].location != NSNotFound) {
                completion ? completion(YES, nil) : 0;
            } else {
                // 不存在
                completion ? completion(NO, [NSError errorWithDomain:@"no installed vpn config" code:0 userInfo:nil]) : 0;
            }
        }
    }];
}

- (void)settingUpVPNConfig:(MPVPNConfigInfo * _Nonnull)config
           completeHandler:(MPVPNManagerCompletionHandler _Nullable )completion {
    
    if (config == nil) {
        completion ? completion(NO,[NSError errorWithDomain:@"Configuration parameters cannot be empty" code:0 userInfo:nil]) : 0 ;
        return;
    }
    _config = config;
    
    [self createKeychainPassword:config.password privateKey:config.sharePrivateKey];
    
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            completion ? completion(NO, error) : 0;
            return;
        }
        
        if (_vpnManager.connection.status == NEVPNStatusConnected) {
            completion ? completion(NO, [NSError errorWithDomain:@"VPN is connected" code:0 userInfo:nil]) : 0;
            return;
        }
        
         NEVPNProtocol *p = [self createVPNProtocol:config];
        _vpnManager.protocol = p;
        _vpnManager.onDemandEnabled = YES;
        _vpnManager.localizedDescription = _config.configTitle;
        _vpnManager.enabled = YES;
        // 保存设置
        [_vpnManager saveToPreferencesWithCompletionHandler:^(NSError *error) {
            if(error) {
                completion ? completion(NO, error) : 0;
            }
            else {
                if (_autoStorageConfig) {
                    [MPCommon saveConfig:_config];
                }
                //                            id conf = [MPCommon getConfig];
                [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                    if (error) {
                        completion ? completion(NO, error) : 0 ;
                        return;
                    }
                    else {
                        completion ? completion(YES, nil) : 0;
                    }
                }];
            }
        }];
        
    }];
}

- (void)removeVPNConfigCompleteHandler:(MPVPNManagerCompletionHandler _Nullable )completion {
    
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"加载 VPN 偏好设置失败 : %@", error);
            completion ? completion(NO, error) : 0;
        } else {
            [_vpnManager removeFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                if (error) {
                    NSLog(@"删除 VPN 偏好设置失败 : %@", error);
                    completion ? completion(NO, error) : 0;
                } else {
                    _vpnManager.protocol = nil;
                    completion ? completion(YES, nil) : 0;
                }
            }];
        }
    }];
}

- (NEVPNProtocol *)createVPNProtocol:(MPVPNConfigInfo *)config {
    switch (config.VPNConnectType) {
            
        case MPVPNConnectTypeIPSec:
        {
            NEVPNProtocolIPSec *p = [NEVPNProtocolIPSec new];
            
            p.username = config.username;
            p.serverAddress = config.serverAddress;
            p.passwordReference = [MPCommon searchKeychainCopyMatching:MPVPNPasswordIdentifier];
            
            if (
                [MPCommon searchKeychainCopyMatching:MPVPNSharePrivateKeyIdentifier] &&
                config.sharePrivateKey) {
                p.authenticationMethod = NEVPNIKEAuthenticationMethodSharedSecret;
                p.sharedSecretReference = [MPCommon searchKeychainCopyMatching:MPVPNSharePrivateKeyIdentifier];
            }
            else if (config.identityData && config.password) {
                p.authenticationMethod = NEVPNIKEAuthenticationMethodCertificate;
                p.identityData = config.identityData;
                p.identityDataPassword = config.identityDataPassword;
            }
            else{
                p.authenticationMethod = NEVPNIKEAuthenticationMethodNone;
            }
            p.localIdentifier = config.localID;
            p.remoteIdentifier = config.remoteID;
            p.useExtendedAuthentication = YES;
            p.disconnectOnSleep = NO;
            
            return p;
        }
            break;
        case MPVPNConnectTypeIKEv2:
        {
            NEVPNProtocolIKEv2 *p = [NEVPNProtocolIKEv2 new];
            p.username = config.username;
            p.passwordReference = [MPCommon searchKeychainCopyMatching:MPVPNPasswordIdentifier];
            
            p.serverAddress = config.serverAddress;
            p.serverCertificateIssuerCommonName = config.serverCertificateCommonName;
            p.serverCertificateCommonName = config.serverCertificateCommonName;
            
            if (
                [MPCommon searchKeychainCopyMatching:MPVPNSharePrivateKeyIdentifier] &&
                config.sharePrivateKey) {
                p.authenticationMethod = NEVPNIKEAuthenticationMethodSharedSecret;
                p.sharedSecretReference = [MPCommon searchKeychainCopyMatching:MPVPNSharePrivateKeyIdentifier];
            }
            else if (config.identityData && config.password) {
                p.authenticationMethod = NEVPNIKEAuthenticationMethodCertificate;
                p.identityData = config.identityData;
                p.identityDataPassword = config.identityDataPassword;
            }
            else{
                p.authenticationMethod = NEVPNIKEAuthenticationMethodNone;
            }
            
            //                    p.identityData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"point-to-client2" ofType:@"p12"]];
            //                    p.identityDataPassword = @"vpnuser";
            
            p.localIdentifier = config.localID;
            p.remoteIdentifier = config.remoteID;
            p.useExtendedAuthentication = YES;
            p.disconnectOnSleep = NO;
            
            //                    NEEvaluateConnectionRule * ru = [[NEEvaluateConnectionRule alloc]
            //                                                     initWithMatchDomains:@[@"google.com"]
            //                                                     andAction:NEEvaluateConnectionRuleActionConnectIfNeeded];
            //
            //                    ru.probeURL = [[NSURL alloc] initWithString:@"http://www.google.com"];
            //
            //                    NEOnDemandRuleEvaluateConnection *ec =[[NEOnDemandRuleEvaluateConnection alloc] init];
            //                    //                ec.interfaceTypeMatch = NEOnDemandRuleInterfaceTypeWiFi;
            //                    [ec setConnectionRules:@[ru]];
            //                    [_vpnManager setOnDemandRules:@[ec]];
            
            
            return p;
        }
            break;
        case MPVPNConnectTypeNone:
            
            break;
    }
    return nil;
}


- (BOOL)mp_isVPNConnected {
    NSError *error;
    NSURL *ipURL = [NSURL URLWithString:@"http://ipof.in/txt"];
    NSString *ip = [NSString stringWithContentsOfURL:ipURL encoding:NSUTF8StringEncoding error:&error];
    if ([ip isEqualToString:_config.serverAddress]) {
        return YES;
    }
    return NO;
}

#pragma mark - 以下三个是网上的一些参考方法判断VPN是否连接 亲测iOS10 无效
- (BOOL)isVPNConnected
{
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            NSString *string = [NSString stringWithFormat:@"%s" , temp_addr->ifa_name];
            NSLog(@"%@",string);
            if ([string rangeOfString:@"tap"].location != NSNotFound ||
                [string rangeOfString:@"tun"].location != NSNotFound ||
                [string rangeOfString:@"ppp"].location != NSNotFound){
                NSLog(@"YES");
                return YES;
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    return NO;
}

- (BOOL)iOS9isVPNConnected
{
    NSDictionary *dict = CFBridgingRelease(CFNetworkCopySystemProxySettings());
    NSArray *keys = [dict[@"__SCOPED__"]allKeys];
    for (NSString *key in keys) {
        if ([key rangeOfString:@"tap"].location != NSNotFound ||
            [key rangeOfString:@"tun"].location != NSNotFound ||
            [key rangeOfString:@"ppp"].location != NSNotFound){
            return YES;
        }
    }
    return NO;
}

- (BOOL)checkForVPNConnectivity
{
    NSDictionary *dict = CFBridgingRelease(CFNetworkCopySystemProxySettings());
    
    return [dict count] > 0;
}



#pragma mark - L2TPTest
- (void)loadL2TPTest{
    NSBundle *b = [NSBundle bundleWithPath:@"/System/Library/Frameworks/NetworkExtension.framework"];
    BOOL success = [b load];
    //    Class NEVPNProtocolL2TP = NSClassFromString(@"NEVPNProtocolL2TP");
//    void *lib =
    dlopen("/System/Library/Frameworks/NetworkExtension.framework", RTLD_LAZY);
    if(success) {
        [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
            
            Class NEVPNProtocolL2TP = NSClassFromString(@"NEVPNProtocolL2TP");
            if (NEVPNProtocolL2TP) {
                NSLog(@"找到当前类");
            }
//            NEVPNProtocolL2TP *p = [[NEVPNProtocolL2TP alloc] init];
            NEVPNProtocol *p = [[NEVPNProtocolL2TP alloc] init];
            
            p.username = @"ZEE";
            [MPCommon createKeychainValue:@"ZEE" forIdentifier:@"L2TPPWD"];
            p.passwordReference = [MPCommon searchKeychainCopyMatching:@"L2TPPWD"];
            p.serverAddress = @"47.91.167.23";
            [MPCommon createKeychainValue:@"vpn" forIdentifier:@"L2TPSS"];
//            p.sharedSecretReference = [MPCommon searchKeychainCopyMatching:@"L2TPSS"];
            
            [p performSelector:@selector(setSharedSecretReference:) withObject:[MPCommon searchKeychainCopyMatching:@"L2TPSS"]];
            p.disconnectOnSleep = NO;
            
            [NEVPNManager sharedManager].protocol = p;
            [NEVPNManager sharedManager].onDemandEnabled = YES;
            [NEVPNManager sharedManager].localizedDescription = @"L2TP";
            [NEVPNManager sharedManager].enabled = YES;
           
//            NEVPNProtocol * rp = [[NEVPNManager sharedManager] protocolConfiguration];
//           BOOL hav = [[NEVPNManager sharedManager] respondsToSelector:@selector(isProtocolTypeValid:)];
//           BOOL hav1 = [rp respondsToSelector:@selector(type)];
//            if (hav1) {
////                [rp performSelector:@selector(type)];
////                rp 
////                NSMethodSignature *signature = [rp instanceMethodSignatureForSelector:@selector(type)];
////                NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
////                invocation.target = rp;
////                invocation.selector = @selector(type);
//            }
//            id a = [rp performSelector:@selector(type)];
            
            NSLog(@"ok");
            
            [ [NEVPNManager sharedManager] saveToPreferencesWithCompletionHandler:^(NSError *error) {
                if(error) {
                    NSLog(@"Save config failed:%@",error);
//                    completeHandle(NO,[NSString stringWithFormat:@"Save config failed [%@]", error.localizedDescription]);
                }
                else {
                    NSLog(@"Save config success");
                    [ [NEVPNManager sharedManager] loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                        if (error) {
                            NSLog(@"re Save config failed:%@",error);
                            return;
                        }
                    }];
                }
            }];
        }];
    }
}



#pragma mark -
#pragma mark - 自动重连 BEGIN
- (void)registerNetWorkReachability{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetWork) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    
}
/**
 *  检测网络
 */
-(void)checkNetWork{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusReachableViaWWAN ||
            status == AFNetworkReachabilityStatusReachableViaWiFi) {
            if (self.vpnManager.connection.status == NEVPNStatusDisconnected) {
                [self start];
            }
        }
    }];
}
#pragma mark - 自动重连 END
- (void)start{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    NSError *startError;
    [_vpnManager.connection startVPNTunnelAndReturnError:&startError];
    if (startError) {
        NSLog(@"Start VPN failed: [%@]", startError.localizedDescription);
    }
    [self outPutConnectionStatus];
    NSLog(@"VPN has start");
}

- (void)outPutConnectionStatus{
    switch (_vpnManager.connection.status) {
        case NEVPNStatusInvalid:
            NSLog(@"NEVPNStatusInvalid The VPN is not configured.");
            break;
        case NEVPNStatusDisconnected:
            NSLog(@"NEVPNStatusDisconnected The VPN is disconnected.");
            break;
        case NEVPNStatusConnecting:
            NSLog(@"NEVPNStatusConnecting The VPN is connecting.");
            break;
        case NEVPNStatusConnected:
            NSLog(@"NEVPNStatusConnected The VPN is connected.");
            break;
        case NEVPNStatusReasserting:
            NSLog(@"NEVPNStatusReasserting The VPN is reconnecting following loss of underlying network connectivity.");
            break;
        case NEVPNStatusDisconnecting:
            NSLog(@"NEVPNStatusDisconnecting The VPN is disconnecting.");
            break;
        default:
            break;
    }
}

- (void)stop{
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    [_vpnManager.connection stopVPNTunnel];
    NSLog(@"VPN has stopped success");
    [self outPutConnectionStatus];
}

- (void)startVPNConnectCompletionHandler:(MPVPNManagerCompletionHandler)completion {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"加载 VPN 偏好设置失败 : %@", error);
            completion ? completion(NO, error) : 0;
        } else {
            NSError *returnError;
            [_vpnManager.connection startVPNTunnelAndReturnError:&returnError];
            if (returnError) {
                NSLog(@"启动 VPN 失败 : %@", returnError);
              completion ? completion(NO, error) : 0;
            } else {
                completion ? completion(YES, nil) : 0;
            }
        }
    }];
}

- (void)stopVPNConnectCompletionHandler:(MPVPNManagerCompletionHandler)completion {
    
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"加载 VPN 偏好设置失败 : %@", error);
            completion ? completion(NO, error) : 0;
        } else {
            [_vpnManager.connection stopVPNTunnel];
            completion ? completion(YES, nil) : 0;
        }
    }];
}



- (void)startMonitoringVPNStatusDidChange:(MPVPNManagerVPNStatusDidChange)VPNStatusDidChange
{
    [[NSNotificationCenter defaultCenter] addObserverForName:NEVPNStatusDidChangeNotification
                                                      object:self.vpnManager.connection
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      VPNStatusDidChange ? VPNStatusDidChange(self.vpnManager.connection.status) : 0;
                                                  }];
}


#pragma mark - gettter 

- (MPVPNConnectType)currentVPNConnectType {
    MPVPNConfigInfo * info = [self currentVPNConnectConfig];
    if (info) {
        return info.VPNConnectType;
    }
    return MPVPNConnectTypeNone;
}

- (NEVPNStatus)currentVPNStatus {
    return _vpnManager.connection.status;
}

- (MPVPNConfigInfo *)currentVPNConnectConfig {
    if ([NSStringFromClass([_vpnManager.protocol class]) isEqualToString:NSStringFromClass([NEVPNProtocolIKEv2 class])])
    {
        MPVPNConfigInfo *info = [MPVPNConfigInfo new];
        NEVPNProtocolIKEv2 *protocol = (NEVPNProtocolIKEv2 *)_vpnManager.protocol;
        info.VPNConnectType = MPVPNConnectTypeIKEv2;
        info.serverAddress = protocol.serverAddress;
        info.username = protocol.username;
        info.remoteID = protocol.remoteIdentifier;
        return info;
    }
    else if ([NSStringFromClass([_vpnManager.protocol class]) isEqualToString:NSStringFromClass([NEVPNProtocol class])])
    {
        MPVPNConfigInfo *info = [MPVPNConfigInfo new];
        NEVPNProtocolIPSec *protocol = (NEVPNProtocolIPSec *)_vpnManager.protocol;
        info.VPNConnectType = MPVPNConnectTypeIPSec;
        info.serverAddress = protocol.serverAddress;
        info.username = protocol.username;
        return info;
    }
    
    return nil;
}

@end

//
//  ViewController.h
//  MPVPNManager
//
//  Created by mopellet on 2017/5/30.
//  Copyright © 2017年 mopellet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NetworkExtension/NetworkExtension.h>

typedef void(^MPVPNManagerCompletionHandler)(BOOL success , NSError * _Nullable error);
typedef void(^MPVPNManagerVPNStatusDidChange)(enum NEVPNStatus status);

/*!
 * @typedef MPVPNConnectType
 * @abstract VPN connect tpey codes
 */
typedef NS_ENUM(NSInteger, MPVPNConnectType){
    MPVPNConnectTypeNone = 0,
    MPVPNConnectTypeIPSec = 1,
    MPVPNConnectTypeIKEv2 = 2,
};


@interface MPVPNConfigInfo : NSObject

@property (nonatomic, assign) MPVPNConnectType VPNConnectType;
/** VPN标题 */
@property (nonatomic, copy, nonnull) NSString *configTitle;
/** 服务器地址 */
@property (nonatomic, copy, nonnull) NSString *serverAddress;
/** 用户名 */
@property (nonatomic, copy, nullable) NSString *username;
/** 密码 */
@property (nonatomic, copy, nullable) NSString *password;
/** vpn验证证书 p12文件  PKCS12 格式 当前属性与sharePrivateKey 仅有一个有效默认为sharePrivateKey优先*/
@property (nonatomic, copy, nullable) NSData *identityData;
/** 证书秘钥 */
@property (nonatomic, copy, nullable) NSString *identityDataPassword;
/** 共享秘钥 */
@property (nonatomic, copy, nullable) NSString *sharePrivateKey;
/** 本地ID */
@property (nonatomic, copy, nullable) NSString *localID;
/** remoteID */
@property (nonatomic, copy, nullable) NSString *remoteID;

/** IKEv2 */
@property (nonatomic, copy, nullable) NSString *serverCertificateIssuerCommonName;
@property (nonatomic, copy, nullable) NSString *serverCertificateCommonName;

@end

/*!
 * @interface MPVPNManager
 * @discussion The MPVPNManager class declares the programmatic interface for an VPN manager.
 *
 * Instances of this class are thread safe.
 */
NS_CLASS_AVAILABLE(10_11, 8_0)
@interface MPVPNManager : NSObject

+ (instancetype _Nonnull )shareInstance;

/*!
 * @method checkVPNConfigCompleteHandler:
 * @discussion This function is used to check VPN config is installed.
 * @param completion A bloc that success or error.
 */
- (void)checkVPNConfigCompleteHandler:(MPVPNManagerCompletionHandler _Nonnull )completion;

/*!
 * @method settingUpVPNConfig:completeHandler:
 * @discussion This function is used to create or update VPN config @see MPVPNConfigInfo.
 * @param config A VPN config @see MPVPNConfigInfo
 * @param completion A bloc that success or error.
 */
- (void)settingUpVPNConfig:(MPVPNConfigInfo * _Nonnull)config
           completeHandler:(MPVPNManagerCompletionHandler _Nullable )completion;

/*!
 * @method removeVPNConfigCompleteHandler:
 * @discussion This function is used to remove VPN config.
 * @param completion A bloc that success or error.
 */
- (void)removeVPNConfigCompleteHandler:(MPVPNManagerCompletionHandler _Nullable )completion;

/*!
 * @method startVPNConnectCompletionHandler:
 * @discussion This function is used to start VPN connect.
 * @param completion A bloc that success or error.
 */
- (void)startVPNConnectCompletionHandler:(MPVPNManagerCompletionHandler _Nullable )completion;

/*!
 * @method stopVPNConnectCompletionHandler:
 * @discussion This function is used to stop VPN connect.
 * @param completion A bloc that success or error.
 */
- (void)stopVPNConnectCompletionHandler:(MPVPNManagerCompletionHandler _Nullable )completion;

/*!
 * @method startMonitoringVPNStatusDidChange:
 * @discussion This function is used to star monitoring VPN status @see NEVPNStatu.
 * @param VPNStatusDidChange A bloc that  VPN status did change.
 */
- (void)startMonitoringVPNStatusDidChange:(MPVPNManagerVPNStatusDidChange _Nullable )VPNStatusDidChange;

/*!
 * @property autoStorageConfig
 * @discussion when create or update VPN sucessful, is not auto storage config in UserDefaults.
 */
@property (nonatomic, assign) BOOL autoStorageConfig;

/*!
 * @property currentVPNConnectType
 * @discussion current VPN connect type @see MPVPNConnectType.
 */
@property (nonatomic, assign, readonly) MPVPNConnectType currentVPNConnectType;

/*!
 * @property currentVPNStatus
 * @discussion current VPN status @see NEVPNStatus.
 */
@property (nonatomic, assign, readonly) NEVPNStatus currentVPNStatus;

/*!
 * @property currentVPNConnectConfig
 * @discussion current VPN connect config @see MPVPNConfigInfo.
 */
@property (nonatomic, strong, readonly, nullable) MPVPNConfigInfo *currentVPNConnectConfig;

/** 在配置setConfig之后调用  iOS10 可用 */
- (BOOL)mp_isVPNConnected;

#pragma mark - 以下三个是网上的一些参考方法判断VPN是否连接 亲测iOS10 无效
- (BOOL)isVPNConnected;
- (BOOL)iOS9isVPNConnected;
- (BOOL)checkForVPNConnectivity;

/**L2TP测试功能 */
- (void)loadL2TPTest;

@end

